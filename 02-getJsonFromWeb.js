var request = new XMLHttpRequest();
var url = "https://dog.ceo/api/breeds/list/all";

request.onreadystatechange = processRequest;
function processRequest() {
  if (request.readyState == 4 && request.status == 200) {
      // show the JSON response in the console
      console.log(request.responseText);
  }
}

request.open("GET", url, true);
request.send();
