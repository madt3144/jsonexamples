// create a JSON object (dictionary)
var myObj = { "name":"John", "age":31, "city":"New York" };

// output each key to the screen
console.log(myObj["age"]);
console.log(myObj.city);
console.log(myObj.name);

// do something with your keys
console.log(myObj.name + myObj.city);

// change a (key,value) pair
myObj.name = "Pritesh";
console.log(myObj.name);

var x = myObj.age = 200 * 3 - 4;
myObj.age = x;
console.log(myObj.age);

// delete a (key, value) pair
delete myObj.name;
console.log(myObj);

// add a simple (key,value) pair
myObj.job = "software developer"
console.log(myObj);

// add a complicated (key, value) pair
myObj.cars = ["honda cr-v", "mazda", "bmw 230i"]
console.log(myObj);

myObj.family = {
  "mother":"Michelle",
  "son":"Peter"
}

console.log(myObj);

// show the mother
console.log(myObj.cars[1]);
console.log(myObj.family.mother);

myObj.cars.push("bicycle");
myObj.family.pets = ["cat", "dog"];

console.log(myObj);
